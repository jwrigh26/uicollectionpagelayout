//
//  LevelCell.swift
//  AngelMoroni
//
//  Created by Justin Wright on 11/14/15.
//  Copyright © 2015 Justin Wright. All rights reserved.
//

import UIKit

class LevelCell: UICollectionViewCell, GameCell {

  @IBOutlet weak var itemNumber: UILabel!
  
  var title: String?{
    didSet {
      itemNumber.text = title
    }
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    //print("We awoke and now need a color")
    
  }
  
  
}
