//
//  StartGameViewController.swift
//  AngelMoroni
//
//  Created by Justin Wright on 11/13/15.
//  Copyright © 2015 Justin Wright. All rights reserved.
//

import UIKit
import SpriteKit

final class LevelMenu: NSObject, Menu {
  
  let layout = SpringBoardFlowLayout()
  
  var collectionView: CollectionView!
  var menuDelegate: MenuDelegate!
  var menuDataSource: MenuDataSource!
  
  private var _collectionView: LevelCollectionView? {
    if let collectionView = self.collectionView as? LevelCollectionView {
      return collectionView
    }
    return nil
  }
  
  convenience init(data: [DataProtocol]?) {
    self.init()
    collectionView = LevelCollectionView(frame: CGRectZero, collectionViewLayout: layout)
    setupCollectionViewDelegate()
    setupCollectionViewDataSource(data)
    _collectionView?.setupLayout()
    
  }
  
  func collectionViewLoaded() {
    scrollToLevel()
  }
  
  func onBack(){
    gameState = .World
  }
  
  func setupCollectionViewDelegate(){
    
    menuDelegate = MenuDelegate{ [weak self] indexPath in
      guard let level = self?.menuDataSource?.data[indexPath.row] as? Level else { return }
      NavigationManager.sharedInstance.currentLevelPath = (level, indexPath)
      self?.gameState = .StartingGame
    }
    
    _collectionView?.delegate = menuDelegate
    
  }
  
  func setupCollectionViewDataSource(data: [DataProtocol]?){
    guard let _ = data else { fatalError("Data is required") }
    
    menuDataSource = MenuDataSource(collectionView: collectionView){
      cell, item in
      guard let cell = cell as? LevelCell, item = item else { return }
      cell.backgroundColor = UIColor.yellowColor()
      cell.title = "\(item.itemNumber)"
    }
    if let world = NavigationManager.sharedInstance.currentWorldPath?.world {
      menuDataSource.data.removeAll(keepCapacity: false)
      menuDataSource.data = world.sortedLevels
    }
    
    _collectionView?.dataSource = menuDataSource
  }
  
  func scrollToLevel(){
    guard let level = NavigationManager.sharedInstance.currentLevelPath,
      layout = _collectionView?.layout as? SpringBoardFlowLayout else { return }
    
    let grid = layout.calculateGridLayout(_collectionView!)
    let item = CGFloat(level.1.row + 1)
    let nbItemsPerScreen = CGFloat(grid.0 * grid.1)
    let screen = floor(item / nbItemsPerScreen)
    let size = GameManager.sharedInstance.screenSize
    let offset = CGPointMake(size.width * screen, 0)
    
    // This was cool but not needed
    //let nbScreens = ceil(CGFloat(menuDataSource.data.count) / CGFloat(grid.0 * grid.1))
    
    _collectionView?.setContentOffset(offset, animated: false)
    NavigationManager.sharedInstance.currentLevelPath = nil
  }
  
}
