//
//  LevelCollectionView.swift
//  AngelMoroni
//
//  Created by Justin Wright on 1/28/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import UIKit

class LevelCollectionView: UICollectionView, CollectionView {
  
  let identifier = "LevelCell"
  let layout: CollectionLayout = SpringBoardFlowLayout()
  
  func setupLayout(){
    guard let layout = self.layout as? SpringBoardFlowLayout else { return }
    layout.frameSize = GameManager.sharedInstance.screenSize
    
    registerNib(UINib(nibName: "LevelCell", bundle: nil), forCellWithReuseIdentifier: identifier)
    
    collectionViewLayout = layout as UICollectionViewLayout
    
  }
  
}
