//
//  BlackCurtain.swift
//  AngelMoroni
//
//  Created by Justin Wright on 1/16/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import UIKit

class BlackCurtain: CALayer {
  
  override init(layer: AnyObject) {
    super.init(layer: layer)
    setup()
  }

  required init?(coder aDecoder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
  }
  
  private func setup(){
    backgroundColor = UIColor.blackColor().CGColor
    frame = UIScreen.mainScreen().bounds
    opacity = 0.0
  }
  
  
  
  
  
  
  
}