//
//  BackButton.swift
//  AngelMoroni
//
//  Created by Justin Wright on 11/27/15.
//  Copyright © 2015 Justin Wright. All rights reserved.
//

import UIKit

class BackButton : UIControl {
  
  let buttonSize = CGSizeMake(86, 64) // 64,48
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setUp()
  }
  
  convenience init(_ screenSize: CGSize, padding: CGFloat = 8) {
    self.init(frame: CGRectZero)
    let position = CGPointMake(padding, screenSize.height - (buttonSize.height+8))
    self.frame = CGRectMake(position.x, position.y, buttonSize.width, buttonSize.height)
    self.sizeToFit()
  }
  
  override func intrinsicContentSize() -> CGSize {
    return CGSizeMake(buttonSize.width, buttonSize.height)
  }
  
  func setUp(){
    self.backgroundColor = UIColor.orangeColor()
  }
  
  override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
    toggleButtonState(touches)
  }
  
  override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
    toggleButtonState(touches, touchEnded: true)
    sendAction(touches)
  }
  
  override func touchesCancelled(touches: Set<UITouch>?, withEvent event: UIEvent?) {
    guard let touches = touches else { return }
    toggleButtonState(touches, touchEnded: true)
  }
  
  func sendAction(touches: Set<NSObject>){
    if touchInside(touches){
      sendActionsForControlEvents(UIControlEvents.TouchUpInside)
    }
  }
  
  func toggleButtonState(touches: Set<NSObject>, touchEnded: Bool = false){
    let touching = touchInside(touches)
    if touching && !touchEnded {
      scaleDownButton()
    }else{
      scaleUpButton()
    }
    
  }
  
  func touchInside(touches: Set<NSObject>)-> Bool {
    if let touch: UITouch = touches.first as? UITouch {
      let touchLocation: CGPoint = touch.locationInView(self)
      if CGRectContainsPoint(self.bounds, touchLocation){
        return true
      }
      //if CGRectContainsPoint(view.convertRect(view.bounds, toCoordinateSpace: self), touchLocation){}
    }
    return false
  }
  
  func scaleDownButton(){
    self.transform = CGAffineTransformMakeScale(0.90, 0.90)
  }
  
  func scaleUpButton(){
    self.transform = CGAffineTransformMakeScale(1.0, 1.0)
  }
  
  
  

}
