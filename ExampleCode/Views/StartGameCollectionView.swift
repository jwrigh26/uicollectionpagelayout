//
//  StartGameCollectionView.swift
//  AngelMoroni
//
//  Created by Justin Wright on 1/16/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import UIKit

class StartGameCollectionView: UICollectionView {
  
//  let worldCellIdentifier = "WorldCell"
//  let levelCellIdentifier = "LevelCell"
//  let worldLayout = CenterCellFlowLayout()
//  let levelLayout = SpringBoardFlowLayout()
//  
//  
//  var isWorldLayout: Bool {
//    return collectionViewLayout == worldLayout ? true : false
//  }
//  
//  var isLevelLayout: Bool {
//    return collectionViewLayout == levelLayout ? true: false
//  }
//  
//  var currentLayout: CollectionLayoutProtocol?
//  
//  var layoutBorderPadding: CGFloat {
//    let screenSize = GameManager.sharedInstance.screenSize
//    return levelLayout.spacingForLayoutReference(screenSize)
//  }
//  
//  private lazy var heightAdjustment: CGFloat = {
//    let height = GameManager.sharedInstance.screenSize.height
//    return height <= 320 ? height * 0.35 : height * 0.28
//  }()
//  
//  
//  func setupConstraints(bottomConstraint: NSLayoutConstraint){
//    bottomConstraint.constant = isWorldLayout ? 0 : heightAdjustment
//  }
//  
//  func setupLayout(){
//    worldLayout.frameSize = GameManager.sharedInstance.screenSize
//    levelLayout.frameSize = GameManager.sharedInstance.screenSize
//    currentLayout = worldLayout
//    //registerClass(WorldCell.self, forCellWithReuseIdentifier: worldCellIdentifier)
//    //registerClass(LevelCell.self, forCellWithReuseIdentifier: levelCellIdentifier)
//    registerNib(UINib(nibName: "WorldCell", bundle: nil), forCellWithReuseIdentifier: worldCellIdentifier)
//    registerNib(UINib(nibName: "LevelCell", bundle: nil), forCellWithReuseIdentifier: levelCellIdentifier)
//    collectionViewLayout = currentLayout as! UICollectionViewLayout
//    //backgroundColor = UIColor.blackColor()
//  }
//  
//  func scrollInsetOffset(){
//    currentLayout?.scrollInsetOffset()
//  }
//  
//  func setCurrentLayoutToLevels(){
//    currentLayout = levelLayout
//  }
//  
//  func setCurrentLayoutToWorlds(){
//    currentLayout = worldLayout
//  }
//  
//  func setCollectionViewLayoutToWorldLayout( complete:( ()->() )?){
//    reloadData()
//    setCollectionViewLayout(worldLayout as UICollectionViewLayout, animated: false){ [weak self] _ in
//      
//      if let indexPaths = self?.indexPathsForVisibleItems() {
//        UIView.performWithoutAnimation{
//          self?.reloadItemsAtIndexPaths(indexPaths)
//          self?.worldLayout.scrollInsetOffset()
//          complete?()
//        }
//      }
//    }
//  }
//  
//  func setCollectionViewLayoutToLevelLayout( complete:( ()->() )?){
//    reloadData()
//    setCollectionViewLayout(levelLayout as UICollectionViewLayout, animated: false){ [weak self] _ in
//      if let indexPaths = self?.indexPathsForVisibleItems() {
//        UIView.performWithoutAnimation{
//          self?.reloadItemsAtIndexPaths(indexPaths)
//          complete?()
//        }
//      }
//    }
//  }
//  
//  // Helper Methods
//  
//  func scrollToLevel(){
//    if let level = GameManager.sharedInstance.currentLevelPath {
//      print("Scroll To Level \(level)")
//      scrollTo(level.1)
//    }
//  }
//  
//  func scrollTo(indexPath: NSIndexPath){
//    //let visibleItems = collectionView.indexPathsForVisibleItems()
//    //let next = NSIndexPath(forRow: visibleItems[0].row, inSection: indexPath.section)
//    scrollToItemAtIndexPath(indexPath, atScrollPosition: UICollectionViewScrollPosition.CenteredHorizontally, animated: false)
//  }
  
}
