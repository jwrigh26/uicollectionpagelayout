//
//  StartScene.swift
//  AngelMoroni
//
//  Created by Justin Wright on 11/13/15.
//  Copyright © 2015 Justin Wright. All rights reserved.
//

import SpriteKit

final class StartScene: BaseScene {
  
  var backgroundLayer: BackgroundLayer!
  
  
  deinit {
  }
  
  override init(size: CGSize) {
    super.init(size: size)
    name = "StartScene"
  }
  
  override func didMoveToView(view: SKView){
    super.didMoveToView(view)
    
    if backgroundLayer != nil {
      delay(0.25){
        self.paused = false
      }
     return
    }
    
    backgroundLayer = BackgroundLayer()
    backgroundLayer.zPosition = -1
    backgroundLayer.accelerateClouds = false
    addChild(backgroundLayer)
  }
  
  
  
  var lastUpdateTime: CFTimeInterval = 0
  var dt: CFTimeInterval = 0
  
  override func update(currentTime: CFTimeInterval) {
    if self.paused { return }
    
    if lastUpdateTime > 0 {
      dt = currentTime - lastUpdateTime
    } else {
      dt = 0
    }
    lastUpdateTime = currentTime
    GameManager.sharedInstance.update(dt)
    backgroundLayer.update(dt)
    
  }
  
  override func willMoveFromView(view: SKView) {
  }
  
  // MARK: Encoding
  required init?(coder aDecoder: NSCoder){
    super.init(coder: aDecoder)
    backgroundLayer = aDecoder.decodeObjectForKey("SSC-backgroundLayer") as! BackgroundLayer
  }
  
  override func encodeWithCoder(aCoder: NSCoder) {
    super.encodeWithCoder(aCoder)
    aCoder.encodeObject(backgroundLayer, forKey: "SSC-backgroundLayer")
  }
}
