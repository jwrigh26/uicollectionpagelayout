//
//  StartGameDelegate.swift
//  AngelMoroni
//
//  Created by Justin Wright on 1/16/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import UIKit

typealias ConfigureCellBlock = (cell: GameCell, item: DataProtocol?) -> ()


class MenuDataSource: NSObject, UICollectionViewDataSource {
  
  let identifier: String
  var data = [DataProtocol]()
  var configureCellBlock: ConfigureCellBlock?
  
  init(collectionView: CollectionView, configureCellBlock: ConfigureCellBlock){
    self.identifier = collectionView.identifier
    self.configureCellBlock = configureCellBlock
    super.init()
  }
  
  func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
    return 1
  }
  
  func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return data.count
  }
  
  func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
    
      let cell = collectionView.dequeueReusableCellWithReuseIdentifier(identifier, forIndexPath: indexPath)
      let item = data[indexPath.row]
      
      if let cell = cell as? GameCell where configureCellBlock != nil {
        configureCellBlock!(cell: cell, item: item)
      }
    
      return cell
  }
  
  
}
