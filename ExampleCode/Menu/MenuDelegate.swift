//
//  MenuDelegate.swift
//  AngelMoroni
//
//  Created by Justin Wright on 1/28/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import UIKit

typealias MenuIndexConfigureBlock = (indexPath: NSIndexPath) -> ()

class MenuDelegate: NSObject, UICollectionViewDelegateFlowLayout {
  
  var menuIndexConfigureBlock: MenuIndexConfigureBlock?
  
  init(block: MenuIndexConfigureBlock){
    self.menuIndexConfigureBlock = block
  }
  
  func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath){
    if menuIndexConfigureBlock != nil {
      return menuIndexConfigureBlock!(indexPath: indexPath)
    }
  }
  
}
