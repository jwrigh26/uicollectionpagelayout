//
//  MenuViewController.swift
//  AngelMoroni
//
//  Created by Justin Wright on 1/28/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import UIKit

class MenuViewController: BaseGameViewController  {
  
  let duration: Double = 0.10
  var delay: Double = 0.0
  var backButton: BackButton!
  var currentMenu: Menu?
  var pauseMenu: PauseMenu?
  var menuData: [DataProtocol]?
  
  var worldMenu: Menu {
    let menu = WorldMenu(data: self.menuData)
    //menu.gameStateDelegate = self
    return menu
  }
  
  var levelMenu: Menu {
    let menu = LevelMenu(data: self.menuData)
    return menu
  }
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    skView.resume()
  }
  
  override func viewWillDisappear(animated: Bool) {
    super.viewWillDisappear(animated)
    skView.pause()
  }
  
  override func viewWillLayoutSubviews() {
    super.viewWillLayoutSubviews()
    currentMenu?.layoutSubViewForMenu()
  }
  
  override func fetchData() {
    menuData = GameData.sharedInstance.worlds
    preloadResources{ [unowned self] in
      self.createMenuScene()
      self.gameState = .WorldNoAnimate
    }
  }
  
  
  func removeMenuIfNecessary(animate: Bool = true, complete: ()->()){
    guard let view = self.currentMenu?.collectionView as? UICollectionView
      where currentMenu != nil else { complete(); return }
    
    let d = animate ? self.duration : 0.0
    
    UIView.animateWithDuration(d, animations: { _ in
      view.alpha = 0.0
    }) { _ in
      self.currentMenu?.cleanup()
      self.currentMenu = nil // reset current
      complete()
    }
  }
  
  func createBackButton(){
    // Create Back Button
    backButton = BackButton(screenSize, padding: 32.0)
    backButton.addTarget(self, action: #selector(MenuViewController.onBackButton), forControlEvents: .TouchUpInside)
    self.view.addSubview(backButton)
  }
  
  func createMenuScene(){
    let scene = StartScene(size: skView.bounds.size)
    if let scene = configureScene(scene) as? StartScene {
      skView.presentScene(scene, transition: transition)
    }
    createBackButton()
  }
  
  
  func transitionMenu(menu: Menu, animate: Bool = true){
    removeMenuIfNecessary(animate){ [weak self] in
      self?.currentMenu = menu
      self?.addMenu(animate)
      self?.collectionViewLoaded()
      self?.fadeInCollectionView(animate)
    }
  }
  
  // MARK: - Actions
  func onBackButton(){
    switch GameManager.sharedInstance.gameState {
    case .World, .Level:
      currentMenu?.onBack()
    case .StartingGame, .Playing:
      delay = 0.60
      backToMenu()
    default:
      break
    }
    
  }
  
  func backToMenu(){
    self.createMenuScene()
    self.gameState = .Level
  }
  
  // MARK: - PauseMenu
  
  func addPauseScreen() {
    if pauseMenu != nil { pauseMenu?.removeFromSuperview() }
    pauseMenu = PauseMenu(parent: self.view)
  }
  
  
  
  
  // MARK: - Private helper methods
  
  private func addMenu(animate: Bool){
    guard let currentMenu = self.currentMenu else { return }
    currentMenu.addCollectionViewToParent(skView)
    
    // Prevent glitch
    if let cv = currentMenu.collectionView as? UICollectionView where animate {
      cv.alpha = 0.0
      print("Yeah!")
    }
    
  }
  
  private func collectionViewLoaded(){
    self.view.layoutIfNeeded() // hog the main queue
    dispatch_async(GlobalMainQueue){ [weak self] in // Make this wait for the main queue
      self?.currentMenu?.collectionViewLoaded()
    }
  }
  
  private func fadeInCollectionView(animate: Bool) {
    guard let cv = self.currentMenu?.collectionView as? UICollectionView where animate else { return }
    cv.alpha = 0.0
    UIView.animateWithDuration(duration, delay: delay, options: .CurveLinear,
                               animations: { _ in cv.alpha = 1.0 },
                               completion: { _ in self.delay = 0 })
    
    
  }
  
  
  
}


