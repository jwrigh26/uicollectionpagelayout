//
//  StartGameModel.swift
//  AngelMoroni
//
//  Created by Justin Wright on 11/27/15.
//  Copyright © 2015 Justin Wright. All rights reserved.
//

import UIKit

typealias dataComplete = ()-> Void

enum DataType {
  case Undefined
  case World
  case Level
}

class MenuModel: NSObject {
  
  var data = [DataProtocol]() {
    didSet {
      dataSource.data = data
    }
  }
  var dataSource: MenuDataSource!
  var dataType: DataType = .Undefined
  
  override init(){
    super.init()
  }
  
  convenience init(dataSource: MenuDataSource, dataType: DataType){
    self.init()
    self.dataSource = dataSource
    self.dataType = dataType
  }
  
 
  
//  func seedData(complete: dataComplete?){
//    DataManager.sharedInstance.seedDataIfNeeded{
//      [weak self] in
//      self?.loadDataItems()
//      complete?()
//    }
//  }
  
  
//  func loadDataItems(){
//    data.removeAll()
//    switch dataType {
//    case .World:
//      data = DataManager.sharedInstance.worlds
//    case .Level:
//      if let world = NavigationManager.sharedInstance.currentWorldPath?.world {
//        data = world.sortedLevels
//      }
//    case .Undefined:
//      print("Data Type should never be undefined")
//      break
//    }
//    dataSource.data = data
//  }
  
}