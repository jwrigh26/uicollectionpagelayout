//
//  Menu.swift
//  AngelMoroni
//
//  Created by Justin Wright on 2/7/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import UIKit


protocol Menu: class {
  
  var menuDelegate: MenuDelegate! { get set}
  var menuDataSource: MenuDataSource! { get set}
  var collectionView: CollectionView! { get set}
  var gameState: GameState { get set }
  
  init(data: [DataProtocol]?)
  init()
  
  func addCollectionViewToParent(parent: UIView)
  func setupCollectionViewDelegate()
  func setupCollectionViewDataSource(data: [DataProtocol]?)
  func cleanup()
  func layoutSubViewForMenu()
  func collectionViewLoaded()
  func hideMenu()
  func onBack()
  
}

extension Menu {
  
  var gameState: GameState {
    get {
      return GameManager.sharedInstance.gameState
    }
    set {
      GameManager.sharedInstance.gameState = newValue
    }
  }
  
  func addCollectionViewToParent(parent: UIView){
    guard let collectionView = collectionView as? UICollectionView else { return }
    
    collectionView.backgroundColor = UIColor.clearColor()
    parent.insertSubview(collectionView, atIndex: 0)
    
    self.collectionView.layoutConstraints()
    
    
  }
  
  func hideMenu(){
    guard let collectionView = collectionView as? UICollectionView else { return }
    collectionView.hidden = true
  }
  
  func showMenu(){
    guard let collectionView = collectionView as? UICollectionView else { return }
    collectionView.hidden = false
  }
  
  func layoutSubViewForMenu(){}
  
  func collectionViewLoaded(){}
  
  func cleanup(){
    self.menuDelegate = nil
    self.menuDataSource = nil
    if let collectionView = self.collectionView as? UICollectionView {
      collectionView.removeFromSuperview()
      self.collectionView = nil
    }
  }
  
  
}
