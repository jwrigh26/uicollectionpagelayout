//
//  StartGameViewController.swift
//  AngelMoroni
//
//  Created by Justin Wright on 11/13/15.
//  Copyright © 2015 Justin Wright. All rights reserved.
//

import UIKit

final class WorldMenu: NSObject, Menu {
  
  let layout = CenterCellFlowLayout()
  var collectionView: CollectionView!
  var menuDelegate: MenuDelegate!
  var menuDataSource: MenuDataSource!
  
  var didScrollToCell = false
  
  private var _collectionView: WorldCollectionView? {
    if let collectionView = self.collectionView as? WorldCollectionView {
      return collectionView
    }
    return nil
  }
  
  convenience init(data: [DataProtocol]?) {
    self.init()
    collectionView = WorldCollectionView(frame: CGRectZero, collectionViewLayout: layout)
    
    setupCollectionViewDelegate()
    setupCollectionViewDataSource(data)
    
    _collectionView?.setupLayout()
  }
  
  func layoutSubViewForMenu(){
    _collectionView?.scrollInsetOffset()
    
  }
  
  func collectionViewLoaded(){
    scrollToWorld()
  }
  
  func onBack(){
    //self.gameStateDelegate?.updateState(.Start)
    //gameState = .Start
    collectionView.scrollTo(NSIndexPath(forRow: 3, inSection: 0))
  }
  
  
  func setupCollectionViewDelegate(){
    
    menuDelegate = MenuDelegate{ [weak self] indexPath in
      guard let world = self?.menuDataSource?.data[indexPath.row] as? World else { return }
      NavigationManager.sharedInstance.currentWorldPath = (world, indexPath)
      self?.gameState = .Level
    }
    
    _collectionView?.delegate = menuDelegate
  }
  
  
  func setupCollectionViewDataSource(data: [DataProtocol]?){
    guard let data = data else { fatalError("Data should never be nil") }
    menuDataSource = MenuDataSource(collectionView: collectionView){
      cell, item in
      guard let cell = cell as? WorldCell, item = item else { return }
      cell.backgroundColor = UIColor.orangeColor()
      cell.title = "\(item.itemNumber)"
    }
    menuDataSource.data.removeAll(keepCapacity: false)
    menuDataSource.data = data.sort{ $0.itemNumber < $1.itemNumber }
    _collectionView?.dataSource = menuDataSource
    
  }
  
  
  func scrollToWorld(){
    if let world = NavigationManager.sharedInstance.currentWorldPath {
      collectionView.scrollTo(world.1)
      NavigationManager.sharedInstance.currentWorldPath = nil
    }
  }
}
