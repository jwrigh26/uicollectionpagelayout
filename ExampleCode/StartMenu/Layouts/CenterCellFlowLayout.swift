//
//  CenterCellFlowLayout.swift
//  AngelMoroni
//
//  Created by Justin Wright on 11/14/15.
//  Copyright © 2015 Justin Wright. All rights reserved.
//

import UIKit

class CenterCellFlowLayout : UICollectionViewFlowLayout, CollectionLayout {
  
  var frameSize: CGSize?
  
  func scrollInsetOffset(){
    guard let width = frameSize?.width, collectionView = self.collectionView else {
      assertionFailure(frameSizeMessage)
      return
    }
    
    // Scroll To Middle
    var insets = collectionView.contentInset
    let value = (width - (width / 2)) * 0.5
    insets.left = value
    insets.right = value
    collectionView.contentInset = insets
    collectionView.decelerationRate = UIScrollViewDecelerationRateNormal // makes a more native feel that is closer to the UIScrollView
    
  }
  
  
  func adjustSpacingForBounds(frame: CGSize, item: CGSize) {
    let spacing: CGFloat = frame.width * 0.10
    minimumLineSpacing = spacing
    minimumInteritemSpacing = spacing
  }
  
  func defineItemSize(frame: CGSize)-> CGSize {
    itemSize = CGSizeMake( frame.width / 2, frame.height / 2)
    return itemSize
  }
  
  override func prepareLayout() {
    super.prepareLayout()
    guard let frameSize = frameSize else {
      assertionFailure(frameSizeMessage)
      return
    }
    collectionView?.showsHorizontalScrollIndicator = false
    collectionView?.showsVerticalScrollIndicator = false
    scrollDirection = .Horizontal
    let size = defineItemSize(frameSize)
    adjustSpacingForBounds(frameSize, item: size)
    collectionView?.pagingEnabled = false
  }
  
  
  
  override func targetContentOffsetForProposedContentOffset(proposedContentOffset: CGPoint, withScrollingVelocity velocity: CGPoint) -> CGPoint {
    
    if let cv = self.collectionView {
      
      let cvBounds = cv.bounds
      let halfWidth = cvBounds.size.width * 0.5;
      let proposedContentOffsetCenterX = proposedContentOffset.x + halfWidth;
      
      // UICollectionViewLayoutAttributes
      if let attributesForVisibleCells = self.layoutAttributesForElementsInRect(cvBounds) {
        
        var candidateAttributes : UICollectionViewLayoutAttributes?
        for attributes in attributesForVisibleCells {
          
          // == Skip comparison with non-cell items (headers and footers) == //
          if attributes.representedElementCategory != UICollectionElementCategory.Cell {
            continue
          }
          
          if let candAttrs = candidateAttributes {
            
            let a = attributes.center.x - proposedContentOffsetCenterX
            let b = candAttrs.center.x - proposedContentOffsetCenterX
            
            if fabsf(Float(a)) < fabsf(Float(b)) {
              candidateAttributes = attributes;
            }
            
          }
          else { // == First time in the loop == //
            
            candidateAttributes = attributes;
            continue;
          }
          
          
        }
        
        // Beautification step , I don't know why it works!
        if(proposedContentOffset.x == -(cv.contentInset.left)) {
          return proposedContentOffset
        }
        
        return CGPoint(x: floor(candidateAttributes!.center.x - halfWidth), y: proposedContentOffset.y)
        
      }
      
      
    }
    
    // fallback
    return super.targetContentOffsetForProposedContentOffset(proposedContentOffset)
  }
  
}
