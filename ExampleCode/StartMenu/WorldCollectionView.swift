//
//  WorldCollectionView.swift
//  AngelMoroni
//
//  Created by Justin Wright on 1/28/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import UIKit

class WorldCollectionView: UICollectionView, CollectionView {
  
  let identifier = "WorldCell"
  let layout: CollectionLayout = CenterCellFlowLayout()
  
  func setupLayout(){
    guard let layout = self.layout as? CenterCellFlowLayout else { return }
    layout.frameSize = GameManager.sharedInstance.screenSize
    
    registerNib(UINib(nibName: "WorldCell", bundle: nil), forCellWithReuseIdentifier: identifier)
    collectionViewLayout = layout as UICollectionViewLayout
  }
  
  func scrollInsetOffset(){
    layout.scrollInsetOffset()
  }
  
  
  
  override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
    super.init(frame: CGRectZero, collectionViewLayout: layout)
  }

  required init?(coder aDecoder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
  }
  
}
